# MS_Import

[![Build Status](https://travis-ci.com/saersamani/CompCreate.jl.svg?branch=master)](https://travis-ci.com/saersamani/MS_Import.jl)




**MS_Import.jl** is a julia package for importing low and high resolution mass spectrometry data. This package is developed with julia v 1.0.5. The current version of the **MS_Import.jl** supports only netCDF and mzXML formats.   

## Installation
### Requirements Windows and macOS

To install and use **MS_Import** you use the julia package manager (i.e. Pkg) and the link to this repository.

### Requirements Linux (Ubuntu 20.04)

To install and use **MS_Import** you first need to install the missing **libz** library and then use the julia package manager (i.e. Pkg) and the link to this repository.
Open the terminal and type:

```bash
    sudo apt-get install zlib1g-dev libncurses5-dev
```
Once, **libz** is installed you can move to the below steps.


### Installation

```julia
using Pkg
Pkg.add(PackageSpec(url="https://bitbucket.org/SSamanipour/ms_import.jl/src/master/"))

```

## Usage

```julia

using MS_Import

mz_values,mz_int,t0,t_end,file_name,pathin,
msModel,msIonisation,msManufacturer,polarity,Rt=import_files_MS1(pathin,filenames) # Only MS1

chrom=import_files(pathin,filenames) # All the channels

```

### Inputs
The **MS_Import** takes four parameters and parses through the file generating a directory with all the relevant fields.

```julia
# Needed inputs
pathin="/path/to/the/folder" # Please insert the path to the folder with the files to be imported.
filenames=["file_name.mzXML"] # Please insert the file name including the extension in the brackets. Also please make sure that there is no "." in the name of the file.

# Optional inputs
mz_thresh=[0,380] # This parameter defines the mass window to be imported. The [0,0] imports the complete file.
int_thersh=50 # This parameter defines the minimum absolute intensity for the datapoint to be imported. The default value is 0 and import the complete file 


```

### Outputs

The **MS_Import** when used for importing MS1 only files outputs six parameters.

```julia
mz_values= "A matrix with measured masses where each row is a scan"
mz_int="An aligned matrix with mz_values containing the detector counts"
t0="The retention of the first scan"
t_end="The retention time of the last scan"
file_name="The name of individual imported file"
pathin="The path to the individual file"
msModel="The model of the MS instrument"
msIonisation="The ionization source used (e.g. electrospray ionization)"
msManufacturer="The manufacturer of the MS instrument"
polarity="The polarity of the first scan of the MS1"
Rt="A vector of retention time in minutes"

or

chrom="A directory having each MS level as a field in the data"

```
The output chromatogram has the below structure:

    + chrom
        + MS1
            + Mz_values: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the measured m/z values.
            + Mz_intensity: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the intensity associated with each measured m/z value.
            + t0: the start of the chromatographic run.
            + t_end: the end of chromatographic run.
            + Polarity: the ionization mode (i.e. "+" and/or "-")
            + TIC: the total ion current
            + BasePeak: the base peak for each scan
            + PrecursorIon: the precursor ion for each scan
            + PrecursorIonWin: the width of the mass window for the precursor ion
            + Rt: the retention time vector.


        + MS2
            + Mz_values: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the measured m/z values.
            + Mz_intensity: an m \* n  matrix with m being the scan numbers and n being the number of mass channels, which contains the intensity associated with each measured m/z value.
            + t0: the start of the chromatographic run.
            + t_end: the end of chromatographic run.
            + Polarity: the ionization mode (i.e. "+" and/or "-")
            + TIC: the total ion current
            + BasePeak: the base peak for each scan
            + PrecursorIon: the precursor ion for each scan
            + PrecursorIonWin: the width of the mass window for the precursor ion.
        + MS_Instrument
            + msModel
            + msIonisation
            + msManufacturer


## Additional options

For removal of the empty MS1 scans you can use function emp_scan_rm!(chrom) whereas for the removal of MS1 that do not have MS2 scans you can use ms2_align!(chrom). 

## Contribution

Issues and pull requests are welcome! Please contact the developers for further information.
