using MS_Import
using Test

@testset "import_files_MS1 (mzXML)" begin
    # Write your own tests here.''
    pathin=""
    filenames=["test_file.mzXML"]
    mz_thresh=[100,150]
    Int_thresh = 100
    mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,
    Rt=import_files_MS1(pathin,filenames,mz_thresh,Int_thresh)
    #println(typeof(mz_vals))
    #println(mz_int)

    @test size(mz_vals,1) > 0

end


@testset "import_files_MS1 (CDF)" begin
    # Write your own tests here.''
    pathin=""
    filenames=["test_file.CDF"]
    mz_thresh=[100,150]
    mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,Rt=import_files_MS1(pathin,filenames,mz_thresh)

    @test size(mz_vals,1) > 0

end


@testset "import_files" begin
    # Write your own tests here.''
    pathin=""
    filenames=["test_file.mzXML"]
    mz_thresh=[100,150]
    chrom=import_files(pathin,filenames,mz_thresh)
    #println(chrom["MS1"]["Rt"])

    @test chrom["MS1"]["Rt"][2] >= 6.1

    mz_thresh=[140,0]
    chrom=import_files(pathin,filenames,mz_thresh)
    @test chrom["MS1"]["Rt"][2] >= 6.1

    # pathin=""
    # filenames=["test_zlib_file.mzXML"]
    # mz_thresh=[100,150]
    # chrom1=import_files(pathin,filenames,mz_thresh)

    # println(chrom1["MS1"]["Rt"])


end
