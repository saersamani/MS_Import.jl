module MS_Import

using Base
#using PyCall
using BenchmarkTools
using NCDatasets
using LightXML
using Unitful
using Codecs
using ProgressMeter
#cd("/")

export import_files, import_files_MS1, import_files_batch_MS1, emp_scan_rm!, ms2_align!, import_files_batch

###############################################################################
## CDF import
function readCDF(File)
    DD=Dict()
    #File="/Users/saersamanipour/Desktop/dev/pkgs/ms_import.jl/test/test_file.CDF"
    ds = Dataset(File)
    scan_acquisition_time=ds["scan_acquisition_time"]
    scan_duration=ds["scan_duration"]                                           #The duration of each scan
    inter_scan_time=ds["inter_scan_time"]                                       #The time between scans
    total_intensity=ds["total_intensity"]                                       #TIC
    mass_range_min=ds["mass_range_min"]                                         #min mass range
    mass_range_max=ds["mass_range_max"]                                         #max mass range
    scan_index=ds["scan_index"]
    point_count=ds["point_count"]
    mass_values = ds["mass_values"]                                             #m/z values
    intensity_values=ds["intensity_values"]                                     #intensity at each m/z value
    #defining the starting point and the end point of the chromatogram
    t0=scan_acquisition_time[1]/60                                              #min
    t_end=scan_acquisition_time[end]/60
    r_t= scan_acquisition_time ./ 60                                      #min
    Max_Mz_displacement=maximum(point_count)
    Mz_values=zeros(length(scan_index),Max_Mz_displacement)
    Mz_intensity=zeros(length(scan_index),Max_Mz_displacement)
    for i=1:length(scan_index)-1
        if (scan_index[i+1] < size(mass_values,1))
            Mz_values[i,1:point_count[i]]=mass_values[scan_index[i]+1:scan_index[i+1]]
            Mz_intensity[i,1:point_count[i]]=intensity_values[scan_index[i]+1:scan_index[i+1]]
        else
            break
        end
        #println(i)
    end
    DD["total_intensity"]=total_intensity
    DD["t0"]=t0
    DD["t_end"]=t_end
    DD["Rt"]=r_t
    DD["Mz_values"]=Mz_values
    DD["Mz_intensity"]=Mz_intensity
    DD["scan_duration"]=scan_duration
    return DD
end
##############################################################################
## The warper function
function cdf_Import(pathin,filenames)
    chrom=Dict()
    for i=1:length(filenames)
        File=joinpath(pathin,filenames[i])
        chrom["MS$i"]=readCDF(File)
    end
    return chrom
end
#####################################################################
## Wrapper to import the files
function import_files(pathin,filenames,mz_thresh=[0,0],Int_thresh=0)
    m=split(filenames[1],".")
    if m[end] == "cdf" || m[end] == "CDF"
        chrom=cdf_Import(pathin,filenames)
    elseif m[end] == "mzxml" || m[end] == "mzXML" || m[end] == "MZXML"
        if isa(filenames,Array)==1
            path_in=joinpath(pathin,filenames[])
        else
            path_in=joinpath(pathin,filenames)
        end
        mz_thresh = Float32.(mz_thresh)
        if mz_thresh[2] == 0.0
            mz_thresh[2] = Inf32
        end
        Int_thresh = Float32(Int_thresh)
        chrom = mzxml_read(path_in,mz_thresh,Int_thresh)
    end
    return (chrom)
end
##########################################################################
# MS1 import
function import_files_MS1(pathin,filenames,mz_thresh=[0,0],Int_thresh=0)
    if isa(filenames,Array)==1
        m=split(filenames[1],".")
    else
        m=split(filenames,".")
    end
    if m[2] == "cdf" || m[2] == "CDF"
        chrom=cdf_Import(pathin,filenames)
        mz_vals=chrom["MS1"]["Mz_values"]
        mz_int=chrom["MS1"]["Mz_intensity"]
        t0=chrom["MS1"]["t0"][1]
        t_end=chrom["MS1"]["t_end"][1]
        Rt=chrom["MS1"]["Rt"]
        msModel="NA"
        msIonisation="NA"
        msManufacturer="NA"
        polarity="NA"
        centroid = "NA"
    elseif m[2] == "mzxml" || m[2] == "mzXML" || m[2] == "MZXML"
        if isa(filenames,Array)==1
            path_in=joinpath(pathin,filenames[])
        else
            path_in=joinpath(pathin,filenames)
        end
        mz_thresh = Float32.(mz_thresh)
        if mz_thresh[2] == 0.0
            mz_thresh[2] = Inf32
        end
        Int_thresh = Float32(Int_thresh)
        chrom = mzxml_read(path_in,mz_thresh,Int_thresh)
        mz_vals=chrom["MS1"]["Mz_values"]
        mz_int=chrom["MS1"]["Mz_intensity"]
        t0=chrom["MS1"]["Rt"][1]
        t_end=chrom["MS1"]["Rt"][end]
        Rt=chrom["MS1"]["Rt"]
        msModel=chrom["MS_Instrument"]["msModel"]
        msIonisation=chrom["MS_Instrument"]["msIonisation"]
        msManufacturer=chrom["MS_Instrument"]["msManufacturer"]
        polarity=chrom["MS1"]["Polarity"][1]
        centroid=chrom["MS1"]["Centroid"]
    end
    return (mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,Rt,centroid)
end
######################################################################
## Import function batch
function import_files_batch(pathin,mz_thresh=[0,0],Int_thresh=0)
    Dname=readdir(pathin)
    ind=zeros(length(Dname),1)
# i=1
    for i=1:length(Dname)
        if Dname[i][end-4:end] == "mzXML" || Dname[i][end-4:end]== "mzxml" || Dname[i][end-4:end] == "MZXML"
            ind[i]=1
        elseif Dname[i][end-3:end] == "CDF" || Dname[i][end-3:end]== "cdf"
            ind[i]=1
        end
    end
    ind_s=findall(x -> x > 0,ind)
    chrom_mat=Array{Any}(undef,size(ind_s,1))
    for i=1:size(ind_s,1)
        filenames=[Dname[ind_s[i]]]
        chrom_mat[i]=import_files(pathin,filenames,mz_thresh,Int_thresh)
    end
    return (chrom_mat)
end
######################################################################
## Import function batch
function import_files_batch_MS1(pathin,mz_thresh=[0,0],Int_thresh=0)
    Dname=readdir(pathin)
    ind=zeros(length(Dname),1)
    @showprogress 1 "Importing the files..." for i=1:length(Dname)
        sleep(0.1)
        if Dname[i][1] != "." && string(Dname[i][1]) != "." && length(Dname[i]) > 5
            if Dname[i][end-4:end] == "mzXML" || Dname[i][end-4:end]== "mzxml" || Dname[i][end-4:end] == "MZXML"
                ind[i]=1
            elseif Dname[i][end-3:end] == "CDF" || Dname[i][end-3:end]== "cdf"
                ind[i]=1
            end
        end
    end
    ind_s=findall(x -> x > 0,ind)
    mz_vales_mat=Array{Any}(undef,size(ind_s,1))
    mz_int_mat=Array{Any}(undef,size(ind_s,1))
    t0_mat=Array{Float64}(undef,size(ind_s,1))
    t_end_mat=Array{Float64}(undef,size(ind_s,1))
    m_mat=Array{Any}(undef,size(ind_s,1))
    Rt_mat=Array{Any}(undef,size(ind_s,1))
    pathin_mat=Array{String}(undef,size(ind_s,1))
    cent_mat = Array{Any}(undef,size(ind_s,1))
    for i=1:size(ind_s,1)
        filenames=[Dname[ind_s[i]]]
        mz_vals,mz_int,t0,t_end,m,pathin,msModel,msIonisation,msManufacturer,polarity,Rt,centroid=import_files_MS1(pathin,filenames,mz_thresh,Int_thresh)
        mz_vales_mat[i]=mz_vals
        mz_int_mat[i]=mz_int
        t0_mat[i]=t0
        t_end_mat[i]=t_end
        m_mat[i]=m[end-1]
        pathin_mat[i]=pathin
        Rt_mat[i]=Rt
        cent_mat[i]= centroid[1]
    end
    return (mz_vales_mat,mz_int_mat,t0_mat,t_end_mat,m_mat,pathin_mat,Rt_mat)
end
###############################################################################
# mzXML read
# path2mzxml = "/Users/saersamanipour/Desktop/dev/Data/12.mzXML"
# path2mzxml = "/Users/saersamanipour/Desktop/dev/ongoing/mzXML.jl/test/test32.mzXML"
function mzxml_read(path2mzxml,mz_thresh::Vector{Float32},Int_thresh::Float32)
    xdoc = parse_file(path2mzxml);
    xroot = root(xdoc);
    if LightXML.name(xroot) != "mzXML"
        error("Not an mzXML file")
    end
    # Find the msRun node
    msRun = find_element(xroot, "msRun");
    el_ins = find_element(msRun, "msInstrument")
    Ins = attribute(find_element(el_ins, "msModel"),"value")
    el_ion = attribute(find_element(el_ins,"msIonisation"),"value")
    el_man = attribute(find_element(el_ins,"msManufacturer"),"value")
    polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int=read_scan(msRun,mz_thresh,Int_thresh)
    chrom=chrom_fold(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
        totIonCurrent,scan_type,centroided,mz,mz_int)
    MS_Instrument=Dict("msModel" => Ins, "msIonisation" => el_ion, "msManufacturer" => el_man)
    chrom["MS_Instrument"]=MS_Instrument
    return chrom
end # function
###############################################################################
# Reading the scans
# Done
function read_scan(msRun,mz_thresh::Vector{Float32},Int_thresh::Float32)
    polarity=Vector{String}(undef, 0)
    pre_mz=Vector{Float32}(undef, 0)
    pre_mz_win=Vector{Float32}(undef, 0)
    retentionTime=Vector{Float32}(undef,0)
    msLevel=Vector{String}(undef, 0)
    basePeakMz=Vector{Float32}(undef, 0)
    totIonCurrent=Vector{Float32}(undef, 0)
    scan_type=Vector{String}(undef, 0)
    centroided=Vector{String}(undef, 0)
    mz=Vector{Vector{Float32}}(undef, 0)
    mz_int=Vector{Vector{Float32}}(undef, 0)
    #c=1
    mz_thresh = Float32.(mz_thresh)
    if mz_thresh[2] == 0.0
        mz_thresh[2] = Inf32
    end
    Int_thresh = Float32(Int_thresh)
    for line in child_elements(msRun)
        if LightXML.name(line) == "scan"
            #println(line)
            #break
            polarity1,pre_mz1,pre_mz_win1,retentionTime1,msLevel1,basePeakMz1,
            totIonCurrent1,scan_type1,centroided1,mz1,I=read_scan_info(line,mz_thresh,Int_thresh)
            polarity=push!(polarity,polarity1)
            pre_mz=push!(pre_mz,pre_mz1)
            pre_mz_win=push!(pre_mz_win,pre_mz_win1)
            retentionTime=push!(retentionTime,retentionTime1)
            msLevel=push!(msLevel,msLevel1)
            basePeakMz=push!(basePeakMz,basePeakMz1)
            totIonCurrent=push!(totIonCurrent,totIonCurrent1)
            scan_type=push!(scan_type,scan_type1)
            centroided=push!(centroided,centroided1)
            mz=push!(mz, mz1)
            mz_int=push!(mz_int, I)
            #c=c+1
            #println(c)
        end
    end
    return(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int)
end # function
###############################################################################
# Extracting info from the scans
# Done
function read_scan_info(line,mz_thresh::Vector{Float32},Int_thresh::Float32)
    # line = find_element(msRun,"scan")
    polarity = attribute(line, "polarity")
    # println(line)
    mm=find_element(line,"precursorMz")
    pre_mz=Float32(0.0)
    pre_mz_win=Float32(0.0)
    if mm !== nothing
        pre_mz=parse(Float32,content(mm))
        if attribute(mm,"windowWideness") !== nothing
            pre_mz_win = parse(Float32,attribute(mm,"windowWideness"))
        end
    end
    tstr=attribute(line, "retentionTime")
    retentionTime = parse(Float32, tstr[3:end-1])
    msLevel = attribute(line, "msLevel")
    basePeakMz = Float32(-100.00)
    if attribute(line, "basePeakMz") !== nothing
        basePeakMz = parse(Float32, attribute(line, "basePeakMz"))
    end
    totIonCurrent = Float32(1.00)
    if attribute(line, "totIonCurrent") !== nothing
       totIonCurrent = parse(Float32, attribute(line, "totIonCurrent"))
    end
    scan_type = attribute(line, "scanType")
    centroided = attribute(line,"centroided")
    peak = find_element(line, "peaks")
    # 1840-element Array{UInt8,1}:
    if attribute(peak, "compressionType") ==  "zlib"
        data = decode(Zlib, decode(Base64, content(peak)))
    else
        data = decode(Base64, content(peak))
    end
    # data = decode(Zlib, decode(Base64, content(peak)))
    # println(bytestring(data))
    A = reinterpret(Float32, data)
    bo = attribute(peak, "byteOrder")
    if bo == "network"
        ntoh!(A)
    else
        error("Don't know what to do with byteOrder $bo")
    end
    I = A[2:2:end]
    mz = A[1:2:end]
    mz1=mz[Int_thresh .<= I]
    I1=I[Int_thresh .<= I]
    mz2=mz1[mz_thresh[1] .<= mz1 .<= mz_thresh[2]]
    I2=I1[mz_thresh[1] .<= mz1 .<= mz_thresh[2]]
    return(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,totIonCurrent,scan_type,centroided,mz2,I2)
end # function
##############################################################################
# XML encoding
function ntoh!(A)
    for i = 1:length(A)
        A[i] = ntoh(A[i])
    end
end
##############################################################################
# Chrom folding
# Done
function chrom_fold(polarity,pre_mz,pre_mz_win,retentionTime,msLevel,basePeakMz,
    totIonCurrent,scan_type,centroided,mz,mz_int)
    chrom=Dict{String, Any}()
    ms_levels::Vector{String}=sort(unique(msLevel))
    
    for i=1:length(ms_levels)
        chrom["MS$i"] = Dict(
            "Rt" => round.(retentionTime[msLevel .== ms_levels[i]]./60, digits=3),
            "Polarity" => polarity[msLevel .== ms_levels[i]],
            "PrecursorIon" => pre_mz[msLevel .== ms_levels[i]],
            "PrecursorIonWin" => pre_mz_win[msLevel .== ms_levels[i]],
            "BasePeak" => basePeakMz[msLevel .== ms_levels[i]],
            "TIC" => totIonCurrent[msLevel .== ms_levels[i]],
            "ScanType" => scan_type[msLevel .== ms_levels[i]],
            "Centroid" => centroided[msLevel .== ms_levels[i]],
            "Mz_values"=> Array{Float32,2}(undef,1,1),
            "Mz_intensity"=>Array{Float32,2}(undef,1,1)
        )
        mz_s=mz[msLevel .== ms_levels[i]]
        mz_int_s=mz_int[msLevel .== ms_levels[i]]
        chrom["MS$i"]["Mz_values"]=array2mat(mz_s)
        chrom["MS$i"]["Mz_intensity"]=array2mat(mz_int_s)
    end
    return chrom
end # function
########################################################################
# Array to matrix
#
# Done
function array2mat(array)::Matrix{Float32}
    
    dummy_var=Matrix{Float32}(undef, size(array,1),maximum(size.(array,1)))
    for i=1:size(array,1)
        dummy_var[i,1:lastindex(array[i])]=array[i]
    end
    GC.gc()
    return dummy_var
end
#############################################################################
# Remove empty scans
function emp_scan_rm!(chrom)
    tv1=sum(chrom["MS1"]["Mz_intensity"], dims=2);
    chrom["MS1"]["Polarity"]=chrom["MS1"]["Polarity"][tv1 .> 0];
    chrom["MS1"]["TIC"]=chrom["MS1"]["TIC"][tv1 .> 0];
    chrom["MS1"]["Rt"]=chrom["MS1"]["Rt"][tv1 .> 0];
    chrom["MS1"]["BasePeak"]=chrom["MS1"]["BasePeak"][tv1 .> 0];
    chrom["MS1"]["PrecursorIon"]=chrom["MS1"]["PrecursorIon"][tv1 .> 0];
    chrom["MS1"]["Centroid"]=chrom["MS1"]["Centroid"][tv1 .> 0];
    chrom["MS1"]["PrecursorIonWin"]=chrom["MS1"]["PrecursorIonWin"][tv1 .> 0];
    chrom["MS1"]["ScanType"]=chrom["MS1"]["ScanType"][tv1 .> 0];
    chrom["MS1"]["Mz_values"]=chrom["MS1"]["Mz_values"][tv1 .> 0,:];
    chrom["MS1"]["Mz_intensity"]=chrom["MS1"]["Mz_intensity"][tv1 .> 0,:];
    return chrom
end # function
#############################################################################
# Removes the MS1 scans without MS2
function ms2_align!(chrom1)
    t_ms2=chrom1["MS2"]["Rt"][1]
    t_end_ms2=chrom1["MS2"]["Rt"][end]
    ind1=argmin(abs.(chrom1["MS1"]["Rt"] .- t_ms2))
    ind2=argmin(abs.(chrom1["MS1"]["Rt"] .- t_end_ms2))
    t_ms1=chrom1["MS1"]["Rt"][ind1]
    t_end_ms1=chrom1["MS1"]["Rt"][ind2]
    ind1_1=argmin(abs.(chrom1["MS2"]["Rt"] .- t_ms1))
    ind2_1=argmin(abs.(chrom1["MS2"]["Rt"] .- t_end_ms1))
    chrom1["MS1"]["Polarity"]=chrom1["MS1"]["Polarity"][ind1:ind2];
    chrom1["MS1"]["TIC"]=chrom1["MS1"]["TIC"][ind1:ind2];
    chrom1["MS1"]["Rt"]=chrom1["MS1"]["Rt"][ind1:ind2];
    chrom1["MS1"]["BasePeak"]=chrom1["MS1"]["BasePeak"][ind1:ind2];
    chrom1["MS1"]["PrecursorIon"]=chrom1["MS1"]["PrecursorIon"][ind1:ind2];
    chrom1["MS1"]["Centroid"]=chrom1["MS1"]["Centroid"][ind1:ind2];
    chrom1["MS1"]["PrecursorIonWin"]=chrom1["MS1"]["PrecursorIonWin"][ind1:ind2];
    chrom1["MS1"]["ScanType"]=chrom1["MS1"]["ScanType"][ind1:ind2];
    chrom1["MS1"]["Mz_values"]=chrom1["MS1"]["Mz_values"][ind1:ind2,:];
    chrom1["MS1"]["Mz_intensity"]=chrom1["MS1"]["Mz_intensity"][ind1:ind2,:];
    chrom1["MS2"]["Polarity"]=chrom1["MS2"]["Polarity"][ind1_1:ind2_1];
    chrom1["MS2"]["TIC"]=chrom1["MS2"]["TIC"][ind1_1:ind2_1];
    chrom1["MS2"]["Rt"]=chrom1["MS2"]["Rt"][ind1_1:ind2_1];
    chrom1["MS2"]["BasePeak"]=chrom1["MS2"]["BasePeak"][ind1_1:ind2_1];
    chrom1["MS2"]["PrecursorIon"]=chrom1["MS2"]["PrecursorIon"][ind1_1:ind2_1];
    chrom1["MS2"]["Centroid"]=chrom1["MS2"]["Centroid"][ind1_1:ind2_1];
    chrom1["MS2"]["PrecursorIonWin"]=chrom1["MS2"]["PrecursorIonWin"][ind1_1:ind2_1];
    chrom1["MS2"]["ScanType"]=chrom1["MS2"]["ScanType"][ind1_1:ind2_1];
    chrom1["MS2"]["Mz_values"]=chrom1["MS2"]["Mz_values"][ind1_1:ind2_1,:];
    chrom1["MS2"]["Mz_intensity"]=chrom1["MS2"]["Mz_intensity"][ind1_1:ind2_1,:];
    return chrom1
end # function
##############################################################################
end # module



##############################################################################